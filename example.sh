CREATE TABLE $TABLE_NAME  (
	 name String,
   sex String
) ENGINE = Kafka SETTINGS kafka_broker_list = '$HOST1:9092, $HOST2:9092, $HOST3:9092',
                            kafka_topic_list = '$TOPICNAME.simpsons',
                            kafka_group_name = '$GROUPNAME.simpsons',
                            kafka_format = 'JSONEachRow';

CREATE TABLE $TABLE_NAME1 ON CLUSTER  streampool (
	 name String,
	 sex String
) ENGINE = ReplicatedMergeTree ('/clickhouse/tables/{layer}/$TABLE_NAME1', '{replica}')
ORDER BY (name);

CREATE MATERIALIZED VIEW materialized_store_data TO $TABLE_NAME
		AS SELECT *
FROM $TABLE_NAME1;
